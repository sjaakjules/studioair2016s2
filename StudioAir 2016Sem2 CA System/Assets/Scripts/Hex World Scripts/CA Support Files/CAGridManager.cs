﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Collections.Generic;

public class CAGridManager : MonoBehaviour {


    [HeaderAttribute("CA Grid Info")]
    public int width;
    public int height;
    public static int _width = 100;
    public static int _height = 100;

    public static int[] _typeCount = new int[HexCell.nTypes];

    public int[] TypeScore = new int[HexCell.nTypes];
    public static int[] _TypeScore = new int[HexCell.nTypes];


    [HeaderAttribute("Grid Setup")]
    Vector3[] TerrainCellPositions;
    static bool isUsingCSVTerrain;
    

    [HeaderAttribute("Main CA Parameters")]
    //The length of time between CA calculation turns
    public static float _refreshrate = 0.3f;
    public float refreshrate = 0.3f;

    public static HexCell[] TerrainHexGrid;
    public static HexCell[] CAGrid;


    void Awake()
    {
        // Static setters
        _refreshrate = refreshrate;

        readTerrain();

        // Set size of the CA according to the CSV or standard size.
        TerrainHexGrid = new HexCell[_height * _width];
        CAGrid = new HexCell[_height * _width];


        // Populate the grid with empty cells
        for (int z = 0; z < _height; z++)
        {
            for (int x = 0; x < _width; x++)
            {
                CreateEmptyCell(TerrainHexGrid, x, z);
                CreateEmptyCell(CAGrid, x, z);
            }
        }

        // Move the cells to the terrain height.
        if (isUsingCSVTerrain)
        {
            for (int i = 0; i < TerrainCellPositions.Length; i++)
            {
                updateHeight(TerrainCellPositions[i]);
            }
        }
    }


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        _refreshrate = refreshrate;

        TypeScore = (int[])_TypeScore.Clone();

        if (!isUsingCSVTerrain)
        {
            _width = width;
            _height = height;
        }
        else
        {
            width = _width;
            height = _height;
        }
    }


    


    // /////////////////////////////////////////////////////////
    //                  Support Functions
    // /////////////////////////////////////////////////////////

    public static void randomPopulate(ref HexCell[] cellGrid, int startingHealth)
    {
        int rndNum1, rndNum2;
        for (int i = 0; i < cellGrid.Length; i++)
        {
            rndNum1 = Random.Range(0, 100);
            if (rndNum1 < Week5_CAmanager._spawnPercent)
            {
                rndNum2 = Random.Range(1, HexCell.nTypes);
                cellGrid[i].changeType((HexType)rndNum2, startingHealth);
            }
        }

    }

    public static void randomPopulateType(ref HexCell[] cellGrid, HexType populationType, int startingHealth)
    {
        int rndNum1;
        for (int i = 0; i < cellGrid.Length; i++)
        {
            rndNum1 = Random.Range(0, 100);
            if (rndNum1 < Week5_CAmanager._spawnPercent && cellGrid[i].type == HexType.Empty)
            {
                cellGrid[i].changeType(populationType, startingHealth);

            }
        }

    }

    public static void dataPopulateType(ref HexCell[] cellGrid, HexType populationType, int startingHealth, float[] dataMap, float targetValue, float valueRange)
    {
        List<int> availableDataNodes = new List<int>();

        for (int i = 0; i < cellGrid.Length; i++)
        {
            if (cellGrid[i].type == HexType.Empty && Mathf.Abs(dataMap[i] - targetValue) < valueRange)
            {
                availableDataNodes.Add(i);
            }
        }

        int rndNum1;
        //availableDataNodes.Shuffle();
        
        for (int i = 0; i < availableDataNodes.Count; i++)
        {
            rndNum1 = Random.Range(0, 100);
            if (rndNum1 < Week5_CAmanager._spawnPercent)
            {
                cellGrid[availableDataNodes[i]].changeType(populationType, startingHealth);

            }
            //cellGrid[i].changeType(populationType, startingHealth);
        }
    }

    public static void CreateEmptyCell(HexCell[] cellArray, int x, int z)
    {
        // Make empty cell from CSV 
        Vector3 position;
        position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
        position.y = 0f;
        position.z = z * (HexMetrics.outerRadius * 1.5f);
        cellArray[HexCoordinates.iFz(x, z)] = new HexCell(position);

        if (x > 0)
        {
            cellArray[HexCoordinates.iFz(x, z)].SetNeighbour(HexDirection.W, cellArray[HexCoordinates.iFz(x, z) - 1]);
        }
        if (z > 0)
        {
            if ((z & 1) == 0) // This will be true for even numbers!!!
            {
                cellArray[HexCoordinates.iFz(x, z)].SetNeighbour(HexDirection.SE, cellArray[HexCoordinates.iFz(x, z) - _width]);
                if (x > 0)
                {
                    cellArray[HexCoordinates.iFz(x, z)].SetNeighbour(HexDirection.SW, cellArray[HexCoordinates.iFz(x, z) - _width - 1]);
                }
            }
            else
            {
                cellArray[HexCoordinates.iFz(x, z)].SetNeighbour(HexDirection.SW, cellArray[HexCoordinates.iFz(x, z) - _width]);
                if (x < _width - 1)
                {
                    cellArray[HexCoordinates.iFz(x, z)].SetNeighbour(HexDirection.SE, cellArray[HexCoordinates.iFz(x, z) - _width + 1]);
                }
            }
        }
    }


    void updateHeight(Vector3 position)
    {
        if (HexCoordinates.iFp(position) >= 0 && HexCoordinates.iFp(position) < _width * _height)
        {
            CAGrid[HexCoordinates.iFp(position)].CellPosition.y = position.y;
            CAGrid[HexCoordinates.iFp(position)].CurrentPosition.y = position.y;
            TerrainHexGrid[HexCoordinates.iFp(position)].CellPosition.y = position.y - 0.2f;
            TerrainHexGrid[HexCoordinates.iFp(position)].CurrentPosition.y = position.y - 0.2f;
            TerrainHexGrid[HexCoordinates.iFp(position)].health = 1;
        }
    }

    public void readTerrain()
    {
        Vector3 offset = Vector3.zero;

        int readWidth = 0;
        int readHeight = 0;

        TerrainCellPositions = CSVManager.readTerrainData(ref offset, ref readWidth, ref readHeight);


        if (TerrainCellPositions != null)
        {
            _width = readWidth;
            _height = readHeight;
            offset.y = -1;
            for (int i = 0; i < TerrainCellPositions.Length; i++)
            {
                TerrainCellPositions[i] = TerrainCellPositions[i] - offset;
            }
            isUsingCSVTerrain = true;
        }
    }
}
