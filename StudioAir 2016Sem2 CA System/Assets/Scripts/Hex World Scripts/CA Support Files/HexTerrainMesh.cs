﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This is used to draw the mesh.
/// An empty mesh is created at wakeup and then using an array of Hex cells drawn using Triangulate.
/// </summary>
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexTerrainMesh : MonoBehaviour
{

    Mesh[] hexMesh;
    List<Vector3>[] vertices;
    List<int>[] triangles;

    MeshCollider meshCollider;
    List<Color>[] colors;

    HexCell[] terrain;
    HexCell neightbour, nextNeighbour;

    Vector3 cellCenter, neighbourCenter, nextNeighbourCenter;

    bool VerticiesFull = true;
    int nMeshes;
    int nverticiesPerCell = 45;

    public Material SpecialMat1;
    private MaterialPropertyBlock block;

    int j = 0;
    int k = 0;

    public float meshScale = 0.2f;

    void Start()
    {
        terrain = CAGridManager.TerrainHexGrid;
        nMeshes = Mathf.CeilToInt(terrain.Length * nverticiesPerCell / (65000-60));
        hexMesh = new Mesh[nMeshes];
        meshCollider = gameObject.AddComponent<MeshCollider>();
        
        vertices = new List<Vector3>[nMeshes+1];
        colors = new List<Color>[nMeshes+1];
        triangles = new List<int>[nMeshes+1];
        CreateHexGridMesh(terrain);

        block = new MaterialPropertyBlock();
    }

    void LateUpdate()
    {
        for (int i = 0; i < nMeshes; i++)
        {
            Graphics.DrawMesh(hexMesh[i], Matrix4x4.identity, SpecialMat1, 0, null, 0, block);
        }
    }

    public void CreateHexGridMesh(HexCell[] cells)
    {
        while (k < cells.Length)
        {
            hexMesh[j] = new Mesh();
            vertices[j] = new List<Vector3>();
            colors[j] = new List<Color>();
            triangles[j] = new List<int>();
            hexMesh[j].name = "Hex Mesh" + j.ToString();

            hexMesh[j].Clear();
            vertices[j].Clear();
            colors[j].Clear();
            triangles[j].Clear();
            while (k < cells.Length)
            {
                Triangulate(cells[k]);
                if (VerticiesFull)
                {
                    k++;
                    break;
                }
                k++;
            }
            hexMesh[j].vertices = vertices[j].ToArray();
            hexMesh[j].colors = colors[j].ToArray();
            hexMesh[j].triangles = triangles[j].ToArray();
            hexMesh[j].RecalculateNormals();
            meshCollider.sharedMesh = hexMesh[j];
            j++;
            VerticiesFull = false;
        }
    }

    void Triangulate(HexCell cell)
    {
        cellCenter = cell.CellPosition;
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(
                cellCenter,
                cellCenter + HexMetrics.corners[i] * meshScale,
                cellCenter + HexMetrics.corners[i + 1] * meshScale
            );
            if (i < 3)
            {
                neightbour = cell.GetNeighbor((HexDirection)i);
                nextNeighbour = cell.GetNeighbor((HexDirection)(i + 1));

                if (neightbour != null)
                {
                    neighbourCenter = neightbour.CellPosition;

                    // If the neighbour is lower
                    if (neighbourCenter.y != cellCenter.y)
                    {
                        AddTriangle(
                        cellCenter + HexMetrics.corners[i] * meshScale,
                        neighbourCenter + HexMetrics.corners[(i + 4) > 6 ? (i + 4 - 6) : i + 4] * meshScale,
                        cellCenter + HexMetrics.corners[i + 1] * meshScale
                        );


                        AddTriangle(
                        cellCenter + HexMetrics.corners[i + 1] / 2,
                        neighbourCenter + HexMetrics.corners[(i + 4) > 6 ? (i + 4 - 6) : i + 4] * meshScale,
                        neighbourCenter + HexMetrics.corners[(i + 3) > 6 ? (i + 3 - 6) : i + 3] * meshScale
                        );
                        
                    }
                }

                if (nextNeighbour != null)
                {
                    nextNeighbourCenter = nextNeighbour.CellPosition;

                    if (neighbourCenter.y != cellCenter.y)
                    {
                        AddTriangle(
                        cellCenter + HexMetrics.corners[i + 1] * meshScale,
                        neighbourCenter + HexMetrics.corners[(i + 3) > 6 ? (i + 3 - 6) : i + 3] * meshScale,
                        nextNeighbourCenter + HexMetrics.corners[(i + 5) > 6 ? (i + 5 - 6) : i + 5] * meshScale
                        );
                    }
                }
            }
        }
    }

    void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        try
        {
            int vertexIndex = vertices[j].Count;
            vertices[j].Add(v1);
            vertices[j].Add(v2);
            vertices[j].Add(v3);
            triangles[j].Add(vertexIndex);
            triangles[j].Add(vertexIndex + 1);
            triangles[j].Add(vertexIndex + 2);
            if (vertices[j].Count > (65000 - 60))
            {
                VerticiesFull = true;
            }
        }
        catch (System.Exception)
        {
            Debug.Log(" Error k: " + k);
            Debug.Log(" Error j: " + j);
            Debug.Log("Vector 1: " + v1.ToString());
            Debug.Log("Vector 2: " + v2.ToString());
            Debug.Log("Vector 3: " + v3.ToString());
        }
    }

    void AddTriangleColor(Color color)
    {
        colors[j].Add(color);
        colors[j].Add(color);
        colors[j].Add(color);
    }
}
