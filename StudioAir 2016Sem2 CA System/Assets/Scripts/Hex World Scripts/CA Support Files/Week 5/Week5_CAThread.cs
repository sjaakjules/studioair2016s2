﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;
using CArules;

namespace Assets.Scripts
{
    enum CAtype
    {
        week5, week6, PartC, custom
    }

    public class Week5_CAThread : GenericWorker
    {
        readonly CAtype caType;
        HexCell[] CAgrid;
        Vector3 playerPosition;
        List<HexCell>[] TypeCells = new List<HexCell>[HexCell.nTypes];
        MovementType[] typeBehaviours;
        MovementType cellBehaviour;

        // //////////////////////////////////////////////////////////////
        //              Plant Bacterial variables
        // //////////////////////////////////////////////////////////////
        int maxHealth = 20;

        // Used for random numbers within the background thread.
        System.Random num;
        

        int[] typeFreq = new int[HexCell.nTypes];
        HexDirection[,] NeighbourTypeDirections = new HexDirection[HexCell.nTypes, 6];           // first indicie is the type, second is the nx,nz position. Use typeFreq for amount of 
        HexDirection[] lowerNeighbour = new HexDirection[6];
        int lowerFreq;
        HexDirection lowestNeighbour = HexDirection.E;
        float[] NeighbourValues = new float[6];
        int[] NeighbourLevels = new int[6];
        int[] NeighbourTypes = new int[6];
        float[] NeighbourHeights = new float[6];


        // These are buffer values so they can be updated while the CA is calculated.
        // Think of it like the public variables to store static variables with unity GUI...
        public int New_maxHealth { private get; set; }
        public Vector3 New_playerPosition { private get; set; }
        public MovementType[] New_typeBehaviour { private get; set; }
        public MovementType New_CellBehaviour { private get; set; }





        // Constructor, used to load the default values.
        public Week5_CAThread(int _maxHealth, MovementType[] _typeBehaviours, MovementType _cellBehaviour)
        {
            for (int i = 0; i < HexCell.nTypes; i++)
            {
                TypeCells[i] = new List<HexCell>(CAGridManager._width * CAGridManager._height);
            }
            maxHealth = _maxHealth;
            typeBehaviours = (MovementType[])_typeBehaviours.Clone();
            cellBehaviour = _cellBehaviour;
            // Buffer values to update dynamically
            New_maxHealth = _maxHealth;
            New_typeBehaviour = (MovementType[])_typeBehaviours.Clone();
            New_CellBehaviour = _cellBehaviour;

            caType = CAtype.week5;
        }

        // This loads the CA which could allow for multiple CA calculations... 
        // but we wont explore this thought any further...
        public bool loadCA()
        {
            if (!this.running)
            {
                CAgrid = CAGridManager.CAGrid;
                return true;
            }
            return false;
        }

        // This starts the loop of a CA calculation, it loads the new values before starting and will only restart once last one ahs finished.
        public new void Start()
        {
            updateCAinfo();
            base.Start();
        }

        public void updateCAinfo()
        {
            maxHealth = New_maxHealth;
            playerPosition = New_playerPosition;
            typeBehaviours = (MovementType[])New_typeBehaviour.Clone();
            cellBehaviour = New_CellBehaviour;
        }

        // This is the main section which is called when base.Start() is called.
        public override void run(BackgroundWorker worker)
        {
            switch (caType)
            {
                case CAtype.week5:
                    AgentMovement();
                    break;
                case CAtype.week6:
                    break;
                case CAtype.PartC:
                    break;
                case CAtype.custom:
                    break;
                default:
                    break;
            }
            
        }

        public void AgentMovement()
        {

            num = new System.Random(System.DateTime.Now.Second * System.DateTime.Now.Millisecond);
            
            


            HexCell neighbour, cell;
            int index;

            float lowestneighbour;


            for (int i = 0; i < HexCell.nTypes; i++)
            {
                CAGridManager._typeCount[i] = TypeCells[i].Count();
                TypeCells[i].Clear();
                CAGridManager._TypeScore[i] = 0;
            }

            //check entire grid
            for (int z = 0; z < CAGridManager._height; z++)
            {
                for (int x = 0; x < CAGridManager._width; x++)
                {
                    index = HexCoordinates.iFz(x, z);
                    cell = CAgrid[index];
                    lowerFreq = 0;



                    if (!cell.hasUpdated)
                    {
                        // Reset counting variables.
                        for (int i = 0; i < typeFreq.Length; i++)
                        {
                            typeFreq[i] = 0;
                        }

                        for (int i = 0; i < 6; i++)
                        {
                            NeighbourValues[i] = float.NaN;
                            NeighbourHeights[i] = float.NaN;
                            NeighbourLevels[i] = -1;
                            NeighbourTypes[i] = -1;
                        }

                        lowestneighbour = -1;

                        foreach (HexDirection neightborDirection in System.Enum.GetValues(typeof(HexDirection)))
                        {
                            neighbour = cell.GetNeighbor(neightborDirection);
                            if (neighbour != null)
                            {
                                NeighbourTypeDirections[(int)neighbour.type, typeFreq[(int)neighbour.type]] = neightborDirection;
                                typeFreq[(int)neighbour.type]++;

                                NeighbourValues[(int)neightborDirection] = Week5_CAmanager.DataMapValues[Week5_CAmanager._selectedDataMap][HexCoordinates.iFp(neighbour.CellPosition)];

                                NeighbourLevels[(int)neightborDirection] = neighbour.level;

                                NeighbourTypes[(int)neightborDirection] = (int)neighbour.type;

                                NeighbourHeights[(int)neightborDirection] = neighbour.CellPosition.y;

                                if (neighbour.CellPosition.y != 0)
                                {
                                    if (neighbour.CellPosition.y - cell.CellPosition.y < 0)
                                    {
                                        lowerNeighbour[lowerFreq] = neightborDirection;
                                        lowerFreq++;
                                        if (lowestneighbour < 0)
                                        {
                                            lowestNeighbour = neightborDirection;
                                        }
                                        else if (neighbour.CellPosition.y < cell.GetNeighbor(lowestNeighbour).CellPosition.y)
                                        {
                                            lowestNeighbour = neightborDirection;
                                        }
                                    }
                                }
                            }
                        }

                        if (cell.type != HexType.Empty)
                        {
                            cell.Movement = cellBehaviour;
                            applyBehaviour(cell.Movement, cell);
                            GrowthCA.growth(cell, num, typeFreq[(int)HexType.Empty], typeFreq[(int)cell.type], NeighbourLevels, typeFreq);
                        }
                        TypeCells[(int)cell.type].Add(cell);
                        CAGridManager._TypeScore[(int)cell.type] += cell.health;
                        cell.hasUpdated = true;

                    }
                }
            }

            // update the new CA values ///////////////////////////////////////////


            //new cell values replace old cell values
            //check entire grid
            for (int z = 0; z < CAGridManager._height; z++)
            {
                for (int x = 0; x < CAGridManager._width; x++)
                {
                    index = HexCoordinates.iFz(x, z);
                    cell = CAgrid[index];
                    cell.health += cell.changeInHealth;
                    cell.changeInHealth = 0;
                    if (cell.health <= 0)
                    {
                        cell.killCell();
                    }
                    else if (cell.health > maxHealth)
                    {
                        //cell.health = maxHealth;
                    }

                    cell.hasUpdated = false;
                }
            }
        }


        void applyBehaviour(MovementType movement, HexCell cell)
        {
            switch (movement)
            {
                case MovementType.Random:
                    AgentCA.MoveRandomly(cell, typeFreq[(int)HexType.Empty], NeighbourTypeDirections, num);
                    break;
                case MovementType.downHill:
                    AgentCA.MoveDownHill(cell, num, NeighbourHeights);
                    break;
                case MovementType.SameDirection:
                    AgentCA.ChangeDirectionRandomly(cell, num);
                    break;
                case MovementType.BetweenValues:
                    AgentCA.MoveBetween(cell, num, NeighbourValues);
                    break;
                case MovementType.TowardsHighValue:
                    AgentCA.MoveHigh(cell, num, NeighbourValues);
                    break;
                case MovementType.TowardsLowValue:
                    AgentCA.MoveLow(cell, num, NeighbourValues);
                    break;
                case MovementType.upHill:
                    AgentCA.MoveUpHill(cell, num, NeighbourHeights);
                    break;
                default:
                    break;
            }
        }
    }
}