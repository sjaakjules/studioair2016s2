﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public enum MovementType
{
    Nothing, Random, downHill, upHill, SameDirection, BetweenValues, TowardsHighValue, TowardsLowValue
}


public enum SeedingType
{
    random, DataMap 
}

/// <summary>
/// This is the original CA manager, 
/// </summary>
public class Week5_CAmanager : MonoBehaviour
{

    //[HeaderAttribute("Cell Type information")]
    HexType CellTypes;


    [HeaderAttribute("Datamaps Parameters")]
    public TextAsset[] DataMaps;
    public int selectedDataMap;


    [HeaderAttribute("Cell Settings:")]
    [Range(0, 100)]
    public int startingHealth = 50;
    [Range(0, 100)]
    public int maxHealth = 100;
    [Range(0, 10)]
    public int maxLevel = 5;


    [HeaderAttribute("CA Seeding Settings:")]
    // CA randomly Generated
    public SeedingType seedingType;
    public bool spawnCells = false;
    HexType spawnCellType;
    public float spawnTargetValue = 2;
    public float spawnValueRange = 0.5f;
    public int spawnPercent = 15;


    [HeaderAttribute("CA Growth Settings:")]
    public MovementType growthBehaviour;
    MovementType[] cellType_GrowthBehaviours = new MovementType[HexCell.nTypes];
    public float growthTargetValue = 2;
    public float growthValueRange = 0.5f;
    [Range(0, 5)]
    public float growthDistribution = 0.5f;



    [HeaderAttribute("Misc Settings:")]
    public Transform playerPosition;



    // Static Variables used for access in other scripts
    public static float[][] DataMapValues;
    public static int _selectedDataMap;
    public static int _maxHealth = 100;
    public static int _maxLevel = 5;
    public static float _growthTargetValue;
    public static float _growthValueRange;
    public static int _spawnPercent;
    public static int _spawnAmount;
    public static float _growthDistribution = 0.5f;

    // System varieables and structure for further development
    Week5_CAThread CAhandler;
    bool isCATypeRandomSeeded = false;
    float elapseTime = 0;           //The length of time between CA calculation turns
    

    void Awake()
    {
        CSVManager._DataMaps = DataMaps;
    }

    // /////////////////////////////////////////////////////////
    //                  Start
    // /////////////////////////////////////////////////////////
    void Start()
    {
        spawnCellType = (HexType)Random.Range(1, HexCell.nTypes);
        DataMapValues = new float[CSVManager._DataMaps.Length][];

        for (int i = 0; i < CSVManager._DataMaps.Length; i++)
        {
            DataMapValues[i] = CSVManager.readDataMapCSV(DataMaps[i]);
        }

        for (int i = 0; i < HexCell.nTypes; i++)
        {
            cellType_GrowthBehaviours[i] = growthBehaviour;
        }

        CAhandler = new Week5_CAThread(maxHealth, cellType_GrowthBehaviours, growthBehaviour);
        
        if (isCATypeRandomSeeded)
        {
            CAGridManager.randomPopulate(ref CAGridManager.CAGrid, startingHealth);
        }

        CAhandler.loadCA();
        

    }


    // /////////////////////////////////////////////////////////
    //                  Loop, Update
    // /////////////////////////////////////////////////////////
    void Update()
    {
        _maxHealth = maxHealth;
        _growthTargetValue = growthTargetValue;
        _growthValueRange = growthValueRange;
        _selectedDataMap = selectedDataMap;
        _maxLevel = maxLevel;
        _spawnPercent = spawnPercent;
        _growthDistribution = growthDistribution;

        elapseTime += Time.deltaTime;
        if (elapseTime > CAGridManager._refreshrate)
        {
            if (!CAhandler.running)
            {
                if (spawnCells)
                {
                    switch (seedingType)
                    {
                        case SeedingType.random:
                            CAGridManager.randomPopulateType(ref CAGridManager.CAGrid, spawnCellType, startingHealth);
                            break;
                        case SeedingType.DataMap:
                            CAGridManager.dataPopulateType(ref CAGridManager.CAGrid, spawnCellType, startingHealth, DataMapValues[selectedDataMap], spawnTargetValue,spawnValueRange);
                            break;
                        default:
                            break;
                    }
                    spawnCells = false;
                }
                // Run the CA after the non CA rules have been applied,
                CAhandler.New_maxHealth = maxHealth;
                CAhandler.New_playerPosition = playerPosition.position;
                CAhandler.New_typeBehaviour = cellType_GrowthBehaviours;
                CAhandler.New_CellBehaviour = growthBehaviour;

                CAhandler.Start();
                //CAhandler.AgentMovement();
                elapseTime = 0;
            }
        }
    }




}
