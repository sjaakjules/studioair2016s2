﻿using UnityEngine;

[System.Serializable]
public struct HexCoordinates
{
    [SerializeField]
    private int x, z;

    public int X
    {
        get
        {
            return x;
        }
    }

    public int Z
    {
        get
        {
            return z;
        }
    }

    public int Y
    {
        get
        {
            return -X - Z;
        }
    }

    public HexCoordinates(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    public override string ToString()
    {
        return "(" + X.ToString() + ", " + Z.ToString() + ", " + Y.ToString() + ")";
    }

    public string ToStringOnSeparateLines()
    {
        return X.ToString() + "\n" + Z.ToString();
    }

    /// <summary>
    /// Get the HexCoordinate using offset X,Z coordiantes. This will work independently of any grid.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public static HexCoordinates FromZigZag(int x, int z)
    {
        return new HexCoordinates(x - z / 2, z);
    }

    /// <summary>
    /// Get the HexCoordinate from the indicie value of the 1D array of HexCells in the HexCAGrid.
    /// This relies on the size of a HexGrid defined in HexCAGrid.width and can return values not within the HexCAGrid
    /// </summary>
    /// <param name="i"></param> This indicie of the cell within the HexCAGrid
    /// <returns></returns>
    public static HexCoordinates FromIndicie(int i)
    {
        return new HexCoordinates((i % CAGridManager._width) - (i / CAGridManager._width) / 2, (i / CAGridManager._width));
    }


    /// <summary>
    /// Get the HexCoordinate that is within the Vector3 Position, Assuming that (0,0,0) centre is at (0,0,0).
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// </summary>
    /// <param name="position"></param> The Vector3 position relative to origin which will be within a HexCoordinate.
    /// <returns></returns>
    public static HexCoordinates FromPosition(Vector3 position)
    {
        float x = position.x / (HexMetrics.innerRadius * 2f);
        float y = -x; 
        float offset = position.z / (HexMetrics.outerRadius * 3f);
        x -= offset;
        y -= offset;
        int iX = Mathf.RoundToInt(x);
        int iY = Mathf.RoundToInt(y);
        int iZ = Mathf.RoundToInt(-x - y);
        if (iX + iY + iZ != 0)
        {
            float dX = Mathf.Abs(x - iX);
            float dY = Mathf.Abs(y - iY);
            float dZ = Mathf.Abs(-x - y - iZ);

            if (dX > dY && dX > dZ)
            {
                iX = -iY - iZ;
            }
            else if (dZ > dY)
            {
                iZ = -iX - iY;
            }
        }
        return new HexCoordinates(iX, iZ);
    }

    /// <summary>
    /// Get the indicie number of the 1D array of HexCells in the HexCAGrid from offset X,Z locations.
    /// This relies on the size of a HexGrid defined in HexCAGrid.width
    /// </summary>
    /// <param name="x"></param> This is offset coordinates, X locations, of a 2D grid of hex cells where max width is 'HexCAGrid.width'
    /// <param name="z"></param> This is offset coordinates, Z locations, of a 2D grid of hex cells where max height is 'HexCAGrid.height'
    /// <returns></returns>
    public static int iFz(int x, int z)
    {
        return x + CAGridManager._width * z;
    }

    /// <summary>
    /// Get the indicie number of the 1D array of HexCells in the HexCAGrid from Hex coordinates.
    /// This relies on the size of a HexGrid defined in HexCAGrid.width
    /// </summary>
    /// <param name="hex"></param> The Hex Coordinate to convert to an indicie within HexCAGrid
    /// <returns></returns>
    public static int iFh(HexCoordinates hex)
    {
        return CAGridManager._width*hex.Z+hex.X+hex.Z/2;
    }

    /// <summary>
    /// Get the indicie number of the 1D array of HexCells in the HexCAGrid from a Vector3 Position. This Assumes that the first cell is at global origin, (0,0).
    /// This relies on the size of a HexGrid defined in HexCAGrid.width
    /// </summary>
    /// <param name="position"></param> The Vector3 Position to convert to an indicie within HexCAGrid
    /// <returns></returns>
    public static int iFp(Vector3 position)
    {
        float x = position.x / (HexMetrics.innerRadius * 2f);
        float y = -x;
        float offset = position.z / (HexMetrics.outerRadius * 3f);
        x -= offset;
        y -= offset;
        int iX = Mathf.RoundToInt(x);
        int iY = Mathf.RoundToInt(y);
        int iZ = Mathf.RoundToInt(-x - y);
        if (iX + iY + iZ != 0)
        {
            float dX = Mathf.Abs(x - iX);
            float dY = Mathf.Abs(y - iY);
            float dZ = Mathf.Abs(-x - y - iZ);

            if (dX > dY && dX > dZ)
            {
                iX = -iY - iZ;
            }
            else if (dZ > dY)
            {
                iZ = -iX - iY;
            }
        }
        return CAGridManager._width * iZ + iX + iZ / 2;
    }

    /// <summary>
    /// Get the vector3 Position of the hex from offset X,Z locations. This Assumes that the first cell is at global origin, (0,0).
    /// Further translation can be applied if needed.
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// </summary>
    /// <param name="x"></param> This is using offset coordinates where the size of the cell width is determined by 'HexMetrics.innerRadius'
    /// <param name="z"></param> This is using offset coordinates where the size of the cell height is determined by 'HexMetrics.outerRadius'
    /// <returns></returns>
    public static Vector3 pFz(int x, int z)
    {
        Vector3 position;
        position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
        position.y = 0f;
        position.z = z * (HexMetrics.outerRadius * 1.5f);
        return position;
    }

    /// <summary>
    /// Gets the vector3 Position of the HexCell within the 1D list of cells in HexCAGrid. This Assumes that the first cell is at global origin, (0,0).
    /// Further translation can be applied if needed.
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// This relies on the size of a HexGrid defined in HexCAGrid.width
    /// </summary>
    /// <param name="i"></param> The indicie value in the 1D list of HexCells within HexCAGrid.
    /// <returns></returns>
    public static Vector3 pFi(int i)
    {
        Vector3 position;
        position.x = (i% CAGridManager._width + i / CAGridManager._width * 0.5f - i / CAGridManager._width / 2) * (HexMetrics.innerRadius * 2f);
        position.y = 0f;
        position.z = i / CAGridManager._width * (HexMetrics.outerRadius * 1.5f);
        return position;
    }

    /// <summary>
    /// Gets the vector3 Postiion of the HexCell from Hex Coordinates. This Assumes that the first cell is at global origin, (0,0).
    /// Further translation can be applied if needed.
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// </summary>
    /// <param name="hex"></param> The HexCoordinate which is converted.
    /// <returns></returns>
    public static Vector3 pFh(HexCoordinates hex)
    {
        Vector3 position;
        position.x = ((hex.X + hex.Z / 2) + hex.Z * 0.5f - hex.Z / 2) * (HexMetrics.innerRadius * 2f);
        position.y = 0f;
        position.z = hex.Z * (HexMetrics.outerRadius * 1.5f);
        return position;
    }
    


}