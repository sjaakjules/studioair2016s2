﻿using UnityEngine;
using System.Collections;

public class HexCell {

    public HexCoordinates coordinates;

    public HexDirection Direction;

    public MovementType Movement;

    public HexType type;

    public int health, changeInHealth, level;

    public bool hasUpdated, isDecaying;

    public Color color;

    public Vector3 CellPosition;
    public Vector3 CurrentPosition;
    
    HexCell[] neighbours = new HexCell[6];

    public static int nTypes {get { return System.Enum.GetNames(typeof(HexType)).Length;}}

    System.Random numGen;

    public HexCell(Vector3 _position, HexType _type, int _Health)
    {
        this.type = _type;
        this.health = _Health;
        this.coordinates = HexCoordinates.FromPosition(_position);
        Direction = (HexDirection)Random.Range(0, 6);
		this.level = 0;
        numGen = new System.Random(Random.Range(int.MinValue, int.MaxValue));
        Movement = MovementType.Random;
    }

    public HexCell(Vector3 _position)
    {
        this.type = HexType.Empty;
        this.health = 0;
        this.coordinates = HexCoordinates.FromPosition(_position);
        this.CellPosition = _position;
        this.CurrentPosition = _position;
        Direction = (HexDirection)Random.Range(0, 6);
        Movement = MovementType.Random;
        this.level = 0;
        numGen = new System.Random(Random.Range(int.MinValue, int.MaxValue));
        changeColour(new Color(0.9f,0.9f,0.95f));
    }


    // Returns the hex cell in the direction specified.
    public HexCell GetNeighbor(HexDirection direction)
    {
        try
        {
            return neighbours[(int)direction];
        }
        catch (System.Exception)
        {
            
        }
        return this;
    }

    /// <summary>
    /// Sets this cell with a neighbor in the direction specified. Also sets this cell as the neighbor in the oposite direction.
    /// </summary>
    /// <param name="direction"></param> Direction from this cell to the neighbor cell passed into this function
    /// <param name="cell"></param> The neighbor cell passed into this function which is a neighbor.
    public void SetNeighbour(HexDirection direction, HexCell cell)
    {
        neighbours[(int)direction] = cell; 
        cell.neighbours[(int)direction.Opposite()] = this;
    }


    /// <summary>
    /// Will swap this cell with the neighbour in the facing direction, "Direction" 
    /// </summary>
    public void Move()
    {
        if (neighbours[(int)Direction] != null)
        {
            neighbours[(int)Direction].swapCell(this);
        }
    }


    /// <summary>
    /// Will grow in the facing direction with a distribution of maxHealth, 1 giving all it's health to the new cell.
    /// </summary>
    public void Grow()
    {
        if (neighbours[(int)Direction] != null && neighbours[(int)Direction].type == HexType.Empty)
        {
            int newHealth = (int)(Week5_CAmanager._maxHealth * Week5_CAmanager._growthDistribution);
            neighbours[(int)Direction].changeType(type, newHealth, this.Direction, this.Movement);
            changeInHealth += -health + (Week5_CAmanager._maxHealth - newHealth);
        }
    }
    

    /// <summary>
    /// This will change the type and health of this cell.
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_Health"></param>
    public void changeType(HexType _type, int _Health)
    {
        this.type = _type;
        this.health = _Health;
        this.changeInHealth = 0;
        this.level = 0;
        this.isDecaying = false;
        this.hasUpdated = true;
        changeColour(getTypeColour(_type));
    }

    /// <summary>
    /// This will change the type and health direction and movement
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_Health"></param>
    public void changeType(HexType _type, int _Health, MovementType _movement)
    {
        this.Movement = _movement;
        changeType(_type, _Health);
    }

    /// <summary>
    /// This will change the type and health direction and movement
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_Health"></param>
    public void changeType(HexType _type, int _Health, HexDirection _direction, MovementType _movement)
    {
        this.Direction = _direction;
        changeType(_type, _Health, _movement);
    }

    /// <summary>
    /// This wwill turn this cell into an empty cell and randomise the direction and movement type.
    /// </summary>
    public void killCell()
    {
        this.type = HexType.Empty;
        this.health = 0;
        this.changeInHealth = 0;
        this.level = 0;
        this.isDecaying = false;
        Direction = (HexDirection)numGen.Next(0, 6);
        Movement = MovementType.Random;
        this.hasUpdated = true;
    }

    /// <summary>
    /// This is used to set all the new perameters, used int swapCell and ReplaceCell
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_Health"></param>
    /// <param name="_ChangeHealth"></param>
    /// <param name="_Direction"></param>
    /// <param name="_level"></param>
    /// <param name="_Movement"></param>
    /// <param name="_isDecaying"></param>
    /// <param name="_CurrentPositon"></param>
    public void changeInfo(HexType _type, int _Health, int _ChangeHealth, HexDirection _Direction, int _level, MovementType _Movement, bool _isDecaying, Vector3 _CurrentPositon)
    {
        this.type = _type;
        this.health = _Health;
        this.changeInHealth = _ChangeHealth;
        this.Direction = _Direction;
        this.level = _level;
        this.Movement = _Movement;
        this.isDecaying = _isDecaying;
        this.CurrentPosition = _CurrentPositon;
        changeColour(getTypeColour(_type));
        this.hasUpdated = true;
    }

    /// <summary>
    /// This will swap this cell with the old cell
    /// </summary>
    /// <param name="oldCell"></param>
    public void swapCell(HexCell oldCell)
    {
        HexType tempType = this.type;
        int tempHealth = this.health;
        int tempChange = this.changeInHealth;
        HexDirection tempDirection = this.Direction;
        Vector3 tempPosition = this.CurrentPosition;
        MovementType tempMovement = this.Movement;
        int tempLevel = this.level;
        bool tempDecay = this.isDecaying;

        this.changeInfo(oldCell.type, oldCell.health, oldCell.changeInHealth, oldCell.Direction, oldCell.level, oldCell.Movement, oldCell.isDecaying, oldCell.CurrentPosition);

        oldCell.changeInfo(tempType, tempHealth, tempChange, tempDirection, tempLevel, tempMovement, tempDecay, tempPosition);
    }

    // This will replace this cell with the old cell and kill the old cell
    public void ReplaceCell(HexCell oldCell)
    {
        this.changeInfo(oldCell.type, oldCell.health, oldCell.changeInHealth, oldCell.Direction, oldCell.level, oldCell.Movement, oldCell.isDecaying, oldCell.CurrentPosition);
        
        oldCell.killCell();
    }

    // This will add the health of the old cell to this one and kill the old one if same type.
    public void AddHealthtoCell(HexCell oldCell)
    {
        if (this.type == oldCell.type)
        {
            this.changeInHealth += oldCell.health;
            oldCell.killCell();
        }
    }
    
    public Color getTypeColour(HexType newType)
    {
        Color colOut;
        switch (newType)
        {
            case HexType.Empty:
                colOut = new Color(0.9f, 0.9f, 0.95f);
                colOut.a = 0.5f;
                return colOut;
            case HexType.Agent:
                colOut = Color.green;
                colOut.a = 0.5f;
                return colOut;
            case HexType.plant:
                colOut = Color.magenta;
                colOut.a = 0.5f;
                return colOut;
            case HexType.Water:
                colOut = Color.blue;
                colOut.a = 0.5f;
                return colOut;
            default:
                colOut = new Color(0.9f, 0.9f, 0.95f);
                colOut.a = 0.5f;
                return colOut;
        }
    }

    public void changeColour(Color colour)
    {
        this.color = colour;
    }
    
}
