﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;


/// <summary>
/// This is the original CA manager, 
/// </summary>
public class PartC_CAmanager : MonoBehaviour
{
    public int startingHealth = 50;
    public int maxHealth = 100;
    public int plantHealthPerTurn = 2;
    public int bacteriaEatingHealthPerTurn = 1;
    public int bacteriaWalkingHealthPerTurn = -2;
    public int bacteriaPlantLifeDrain = -2;
    public int randomSeedPercent = 5;
    public int rainPercent = 5;

    [HeaderAttribute("Main CA Parameters")]
    // CA randomly Generated
    public bool isCARandomSeeded = true;

    //The length of time between CA calculation turns
    public float refreshrate = 0f;
    float elapseTime = 0;
    public HexType populationType;
    public bool willrandomPopulate = false;

    //HexMesh hexMesh;
    

    public static HexCell[] HexGridUpdated;

    Part_CAThread CAhandler;


    // /////////////////////////////////////////////////////////
    //                  Start
    // /////////////////////////////////////////////////////////

    void Start()
    {
        CAhandler = new Part_CAThread(maxHealth, plantHealthPerTurn, bacteriaEatingHealthPerTurn, bacteriaWalkingHealthPerTurn, bacteriaPlantLifeDrain);
        
        HexGridUpdated = new HexCell[CAGridManager._height * CAGridManager._width];

        for (int z = 0; z < CAGridManager._height; z++)
        {
            for (int x = 0; x < CAGridManager._width; x++)
            {
                CAGridManager.CreateEmptyCell(CAGridManager.TerrainHexGrid, x, z);
                CAGridManager.CreateEmptyCell(HexGridUpdated, x, z);
            }
        }

        for (int i = 0; i < CAGridManager.TerrainHexGrid.Length; i++)
        {
            HexGridUpdated[HexCoordinates.iFp(CAGridManager.TerrainHexGrid[i].CellPosition)].CellPosition.y = CAGridManager.TerrainHexGrid[i].CellPosition.y + 0.2f;
        }

        if (isCARandomSeeded)
        {
           // CAmanager.randomPopulate(ref HexGridUpdated, startingHealth);
        }

        CAhandler.loadCA(this);


    }
    

    // /////////////////////////////////////////////////////////
    //                  Loop, Update
    // /////////////////////////////////////////////////////////
    void Update()
    {
        elapseTime += Time.deltaTime;
        if (elapseTime > refreshrate)
        {
            if (!CAhandler.running)
            {
                CAGridManager.randomPopulateType(ref HexGridUpdated, populationType, startingHealth);
                // Run the CA after the non CA rules have been applied,
                CAhandler.Start();
                elapseTime = 0;
            }
        }
    }


    

}
