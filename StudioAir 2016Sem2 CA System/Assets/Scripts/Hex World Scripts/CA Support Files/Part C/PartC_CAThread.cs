﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;
using CArules;

namespace Assets.Scripts
{
    public class Part_CAThread : GenericWorker
    {
        PartC_CAmanager CAGridInfo;
        HexCell[] CAgrid;
        Vector3 playerPosition;
        List<HexCell> EmptyTypes = new List<HexCell>(CAGridManager._width * CAGridManager._height + 10);
        List<HexCell> AgentTypes = new List<HexCell>(CAGridManager._width * CAGridManager._height + 10);
        List<HexCell> PlagueTypes = new List<HexCell>(CAGridManager._width * CAGridManager._height + 10);
        List<HexCell> WaterTypes = new List<HexCell>(CAGridManager._width * CAGridManager._height + 10);

        // //////////////////////////////////////////////////////////////
        //              Plant Bacterial variables
        // //////////////////////////////////////////////////////////////
        int maxHealth = 20;
        int plantHealthPerTurn = 2;
        int bacteriaEatingHealthPerTurn = 1;
        int bacteriaWalkingHealthPerTurn = -2;
        int bacteriaPlantLifeDrain = -1;
        
        // Used for random numbers within the background thread.
        System.Random num;
        

        // These are buffer values so they can be updated while the CA is calculated.
        // Think of it like the public variables to store static variables with unity GUI...
        public int New_maxHealth { private get; set; }
        public int New_plantHealthPerTurn { private get; set; }
        public int New_bacteriaEatingHealthPerTurn { private get; set; }
        public int New_bacteriaWalkingHealthPerTurn { private get; set; }
        public int New_bacteriaPlantLifeDrain { private get; set; }
        public Vector3 New_playerPosition { private get; set; }



        // Constructor, used to load the default values.
        public Part_CAThread(int _maxHealth, int _plantHealthPerTurn, int _bacteriaEatingHealthPerTurn, int _bacteriaWalkingHealthPerTurn, int _bacteriaPlantLifeDrain)
        {

            maxHealth = _maxHealth;
            plantHealthPerTurn = _plantHealthPerTurn;
            bacteriaEatingHealthPerTurn = _bacteriaEatingHealthPerTurn;
            bacteriaWalkingHealthPerTurn = _bacteriaWalkingHealthPerTurn;
            bacteriaPlantLifeDrain = _bacteriaPlantLifeDrain;

            // Buffer values to update dynamically
            New_maxHealth = _maxHealth;
            New_plantHealthPerTurn = _plantHealthPerTurn;
            New_bacteriaEatingHealthPerTurn = _bacteriaEatingHealthPerTurn;
            New_bacteriaWalkingHealthPerTurn = _bacteriaWalkingHealthPerTurn;
            New_bacteriaPlantLifeDrain = _bacteriaPlantLifeDrain;
        }

        // This loads the CA which could allow for multiple CA calculations... 
        // but we wont explore this thought any further...
        public bool loadCA(PartC_CAmanager CAGrid)
        {
            if (!this.running)
            {
                CAGridInfo = CAGrid;
                CAgrid = PartC_CAmanager.HexGridUpdated;
                return true;
            }
            return false;
        }

        // This starts the loop of a CA calculation, it loads the new values before starting and will only restart once last one ahs finished.
        public new void Start()
        {
            updateCAinfo();
            base.Start();
        }

        public void updateCAinfo()
        {
            maxHealth = New_maxHealth;
            plantHealthPerTurn = New_plantHealthPerTurn;
            bacteriaEatingHealthPerTurn = New_bacteriaEatingHealthPerTurn;
            bacteriaWalkingHealthPerTurn = New_bacteriaWalkingHealthPerTurn;
            bacteriaPlantLifeDrain = New_bacteriaPlantLifeDrain;
            playerPosition = New_playerPosition;
        }

        // This is the main section which is called when base.Start() is called.
        public override void run(BackgroundWorker worker)
        {
            plantsVsBacteria();

            //tempGrid = (HexCell[])CAgrid.Clone();
            //CAgrid = (HexCell[])HexCAGrid.HexGridUpdated.Clone(); 
            //HexCAGrid.HexGridUpdated = (HexCell[])tempGrid.Clone();
        }

        public void plantsVsBacteria()
        {

            num = new System.Random(System.DateTime.Now.Second * System.DateTime.Now.Millisecond);

            //stuff used when checking CA values for every cell
            int nTypes = System.Enum.GetNames(typeof(HexType)).Length;
            int[] typeFreq = new int[nTypes];

            HexDirection[,] NeighbourTypeDirections = new HexDirection[nTypes, 6];           // first indicie is the type, second is the nx,nz position. Use typeFreq for amount of 

            HexCell neighbour, cell;
            int index;

            HexDirection[] lowerNeighbour = new HexDirection[6];
            int lowerFreq;
            HexDirection lowestNeighbour = HexDirection.NE;
            float lowestHeight;

            AgentTypes.Clear();
            EmptyTypes.Clear();
            PlagueTypes.Clear();
            WaterTypes.Clear();

            //check entire grid
            for (int z = 0; z < CAGridManager._height; z++)
            {
                for (int x = 0; x < CAGridManager._width; x++)
                {
                    index = HexCoordinates.iFz(x, z);
                    cell = CAgrid[index];
                    lowerFreq = 0;

                    

                    if (!cell.hasUpdated)
                    {
                        // Reset counting variables.
                        for (int i = 0; i < typeFreq.Length; i++)
                        {
                            typeFreq[i] = 0;
                        }

                        lowestHeight = 9999f;
                        foreach (HexDirection neightborDirection in System.Enum.GetValues(typeof(HexDirection)))
                        {
                            neighbour = cell.GetNeighbor(neightborDirection);
                            if (neighbour != null)
                            {
                                NeighbourTypeDirections[(int)neighbour.type, typeFreq[(int)neighbour.type]] = neightborDirection;
                                typeFreq[(int)neighbour.type]++;

                                if (neighbour.CellPosition.y != 0)
                                {
                                    if (neighbour.CellPosition.y - cell.CellPosition.y < 0.1)
                                    {
                                        lowerNeighbour[lowerFreq] = neightborDirection;
                                        lowerFreq++;
                                    }

                                    if (neighbour.CellPosition.y < lowestHeight)
                                    {
                                        lowestHeight = neighbour.CellPosition.y;
                                        lowestNeighbour = neightborDirection;
                                    }
                                }
                            }
                        }

                        switch (cell.type)
                        {
                            // /////////////////////////////////////////////////////////////////
                            //                      If the cell is empty
                            // /////////////////////////////////////////////////////////////////
                            case HexType.Empty:
                                EmptyTypes.Add(cell);

                                break;
                            // /////////////////////////////////////////////////////////////////
                            //                      If the cell is plant
                            // /////////////////////////////////////////////////////////////////
                            case HexType.Agent:
                                AgentTypes.Add(cell);

                                PartCRules.PlagueRules(cell, typeFreq, NeighbourTypeDirections, num, plantHealthPerTurn, maxHealth);
                                break;

                            // /////////////////////////////////////////////////////////////////
                            //                      If the cell is Bacteria
                            // /////////////////////////////////////////////////////////////////
                            case HexType.plant:
                                PlagueTypes.Add(cell);

                                PartCRules.AgentRules(cell, typeFreq, NeighbourTypeDirections, num, bacteriaWalkingHealthPerTurn, bacteriaEatingHealthPerTurn, bacteriaPlantLifeDrain, maxHealth);
                                break;

                            // /////////////////////////////////////////////////////////////////
                            //                      If the cell is Water
                            // /////////////////////////////////////////////////////////////////
                            case HexType.Water:
                                WaterTypes.Add(cell);

                                AgentCA.ChangeDirectionRandomly(cell, num);
                                //AgentCA.MoveRandomly(cell, typeFreq, NeighbourTypeDirections, num);

                                //PartCRules.WaterMovement(cell, lowerFreq, lowerNeighbour, lowestNeighbour, typeFreq, lowestHeight, NeighbourTypeDirections, num);
                                break;
                            default:
                                break;
                        }

                        cell.hasUpdated = true;
                        
                    }
                }
            }

            // update the new CA values ///////////////////////////////////////////


            //new cell values replace old cell values
            //check entire grid
            for (int z = 0; z < CAGridManager._height; z++)
            {
                for (int x = 0; x < CAGridManager._width; x++)
                {
                    index = HexCoordinates.iFz(x, z);
                    cell = CAgrid[index];
                    cell.health += cell.changeInHealth;

                    if (cell.health <= 0)
                    {
                        cell.changeType(HexType.Empty, 0);
                    }
                    else if (cell.health > maxHealth)
                    {
                        cell.health = maxHealth;
                    }

                    cell.hasUpdated = false;
                }
            }

        }
    }
}