﻿using UnityEngine;
using System.Collections;

public class HexDrawMesh : MonoBehaviour {

    public GameObject player;
    private Mesh mesh;

    [Header("Do Voxels types have unique geometries?")]
    bool uniqueGeom;
    public GameObject[] voxelGeomPerLevel = new GameObject[Week5_CAmanager._maxLevel];

    //[HideInInspector]
    Mesh[] meshPerType;
    Mesh[] meshPerlevel;

    [Header("What materials do each voxel type use?")]
    public GameObject Voxmesh;
    public Material[] voxelTypeMaterials = new Material[HexCell.nTypes];
    public Material SpecialMat1;
    public Material SpecialMat2;

    [Header("How many voxels are drawn on screen?")]
    public float setRenderDist;
    public static float renderDist;

    //IGNORE
    private MaterialPropertyBlock block;
    private int colorID;
    float cap;
    float health;
    HexCell CAcell;
    HexCell groundcell;

    Vector3 currentPos;

    Matrix4x4 voxTransform = Matrix4x4.identity;
    Matrix4x4 BaseElevationTransform = Matrix4x4.identity;
    float distanceToGroundCell;
    float distanceToCACell;

    float chancePercent;
    public float groundRenderMultiplyer = 1.5f;

    // SETUP - IGNORE
    void Start()
    {
        meshPerlevel = new Mesh[Week5_CAmanager._maxLevel];

        block = new MaterialPropertyBlock();
        colorID = Shader.PropertyToID("_Color");
        cap = 0;

        for (int i = 0; i < HexCell.nTypes; i++)
        {
            //meshPerlevel[i] = voxelGeomPerLevel[i].GetComponent<MeshFilter>().sharedMesh;
        }

        mesh = Voxmesh.GetComponent<MeshFilter>().sharedMesh;



    }


    // Update is called once per frame
    void Update()
    {
        previewMaterials();
        renderDist = setRenderDist;


        for (int x = 0; x < CAGridManager._width; x++)
        {
            for (int z = 0; z < CAGridManager._height; z++)
            {

                CAcell = CAGridManager.CAGrid[HexCoordinates.iFz(x, z)];

                if (CAcell.type != HexType.Empty)
                {
                    CAcell.CurrentPosition = Vector3.Lerp(CAcell.CurrentPosition, CAcell.CellPosition, Time.deltaTime);
                    distanceToCACell = Vector3.Distance(player.transform.position, CAcell.CellPosition);
                    
                    chancePercent = Random.Range(0f, 1f);

                    float health = (1.0f * CAcell.health / Week5_CAmanager._maxHealth) > 1000 ? 1000 : (1.0f * CAcell.health / Week5_CAmanager._maxHealth);

                    Vector3 healthScaleVector = new Vector3(80f, CAcell.level, 80f) * .01f;
                    Vector3 sizeScaleVector = new Vector3(1, CAcell.level + 1, 1) * 0.8f;

                    voxTransform.SetTRS(CAcell.CurrentPosition, Quaternion.identity, sizeScaleVector);

                    if (distanceToCACell < renderDist)
                    {
                        if (CAcell.health > cap)
                        {

                            if (uniqueGeom == false)
                            {
                                Graphics.DrawMesh(mesh, voxTransform, voxelTypeMaterials[(int)CAcell.type], 0, null, 0, block);
                            }

                            if (uniqueGeom == true)
                            {
                                Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, voxelTypeMaterials[(int)CAcell.type], 0, null, 0, block);
                            }
                        }
                    }
                    else if (distanceToCACell < renderDist * 2 && ((distanceToCACell - renderDist) / (renderDist)) < chancePercent)
                    {
                        if (CAcell.health > cap)
                        {
                            CAcell.CurrentPosition = Vector3.Lerp(CAcell.CurrentPosition, CAcell.CellPosition, Time.deltaTime);

                            if (uniqueGeom == false)
                            {
                                Graphics.DrawMesh(mesh, voxTransform, voxelTypeMaterials[(int)CAcell.type], 0, null, 0, block);
                            }

                            if (uniqueGeom == true)
                            {
                                Graphics.DrawMesh(meshPerType[(int)CAcell.type], voxTransform, voxelTypeMaterials[(int)CAcell.type], 0, null, 0, block);
                            }
                        }
                    }
                }
            }
        }
    }

    void previewMaterials()
    {
        for (int i = 0; i < voxelTypeMaterials.Length; i++)
        {
            Vector3 currentPos = new Vector3(20 + i * 2, 1, 20);
            Graphics.DrawMesh(mesh, currentPos, Quaternion.identity, voxelTypeMaterials[i], 0, null, 0, block);
        }
    }

}
