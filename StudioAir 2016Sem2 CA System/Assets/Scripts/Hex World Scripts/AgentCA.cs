﻿using UnityEngine;using System.Collections;using System.Collections.Generic;using System.Linq;public class AgentCA : MonoBehaviour{    // Use this for initialization    void Start()    {    }    // Update is called once per frame    void Update()    {    }    public static void MoveBetween(HexCell cell, System.Random num, float[] NeighbourAttributeValues)    {        float minDistance = NeighbourAttributeValues.Min(n => Mathf.Abs(Week5_CAmanager._growthTargetValue - n));        if (minDistance < Week5_CAmanager._growthValueRange)        {            int directionIndex = NeighbourAttributeValues.ToList().IndexOf(NeighbourAttributeValues.First(n => Mathf.Abs(Week5_CAmanager._growthTargetValue - n) == minDistance));            cell.Direction = (HexDirection)directionIndex;        }        // Make sure your not near the edge of the map        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }        /*         *          * ---------------- Depreciated ------------------        // Move if there is nothing in front of you, otherwise join if same cell        if (cell.GetNeighbor(cell.Direction).type == HexType.Empty)        {            (cell.GetNeighbor(cell.Direction)).ReplaceCell(cell);        }        else if (cell.GetNeighbor(cell.Direction).type == cell.type)        {            cell.GetNeighbor(cell.Direction).AddHealthtoCell(cell);        }        *         *         */    }    public static void MoveLow(HexCell cell, System.Random num, float[] NeighbourAttributeValues)    {        int minDirection = NeighbourAttributeValues.ToList().IndexOf(NeighbourAttributeValues.Min());        cell.Direction = (HexDirection)minDirection;        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }


        /*         *          * ---------------- Depreciated ------------------        if (cell.GetNeighbor(cell.Direction).type == HexType.Empty)        {            (cell.GetNeighbor(cell.Direction)).ReplaceCell(cell);        }        else if (cell.GetNeighbor(cell.Direction).type == cell.type)        {            cell.GetNeighbor(cell.Direction).AddHealthtoCell(cell);        }        *
        *
        */
    }    public static void MoveHigh(HexCell cell, System.Random num, float[] NeighbourAttributeValues)    {        int maxDirection = NeighbourAttributeValues.ToList().IndexOf(NeighbourAttributeValues.Max());        cell.Direction = (HexDirection)maxDirection;        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }

        /*         *          * ---------------- Depreciated ------------------        if (cell.GetNeighbor(cell.Direction).type == HexType.Empty)        {            (cell.GetNeighbor(cell.Direction)).ReplaceCell(cell);        }        else if (cell.GetNeighbor(cell.Direction).type == cell.type)        {            cell.GetNeighbor(cell.Direction).AddHealthtoCell(cell);        }        *
        *
        */
    }    public static void MoveDownHill(HexCell cell, System.Random num, float[] NeighbourHeights)    {        //List<float> lowerCells = NeighbourHeights.ToList().FindAll(x => cell.CellPosition.y > x && x != float.NaN);        List<Vector2> lowerDirections = new List<Vector2>();        for (int i = 0; i < NeighbourHeights.Length; i++)
        {
            if (cell.CellPosition.y > NeighbourHeights[i] && NeighbourHeights[i] != float.NaN)
            {
                lowerDirections.Add(new Vector2(i, NeighbourHeights[i]));
            }
        }
        lowerDirections.Sort((a,b) => a.y.CompareTo(b.y));        HexDirection lowerDirection;        for (int i = 0; i < lowerDirections.Count; i++)
        {
            lowerDirection = (HexDirection)(int)lowerDirections[i].x;
            if (cell.GetNeighbor(lowerDirection) == null && cell.GetNeighbor(lowerDirection).type == HexType.Empty)
            {
                cell.Direction = lowerDirection;
                break;
            }
        }        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }    }
    public static void MoveOnlyUpHill(HexCell cell, System.Random num, float[] NeighbourHeights)
    {
        List<float> higherCells = NeighbourHeights.ToList().FindAll(x => cell.CellPosition.y < x && x != float.NaN);
        float[] higherCellsOrded = higherCells.OrderByDescending(x => x).ToArray();
        HexDirection higherDirection = (HexDirection)(NeighbourHeights.ToList()).IndexOf(higherCellsOrded[0]);
        cell.Direction = higherDirection;
        for (int i = 0; i < higherCellsOrded.Length; i++)
        {
            higherDirection = (HexDirection)(NeighbourHeights.ToList()).IndexOf(higherCellsOrded[i]);
            if (cell.GetNeighbor(higherDirection) != null && cell.GetNeighbor(higherDirection).type == HexType.Empty)
            {
                cell.Direction = higherDirection;
                break;
            }
        }
    }

    public static void MoveUpHill(HexCell cell, System.Random num, float[] NeighbourHeights)    {        //List<float> higherCells = NeighbourHeights.ToList().FindAll(x => cell.CellPosition.y < x && x != float.NaN);                 List<Vector2> higherDirection = new List<Vector2>();            // Create an empty v2 list, x component is the neighbour direction and the y is height                                                                        // Get all the neighbours which are higher,         for (int i = 0; i < NeighbourHeights.Length; i++)               // add the neighbour direction and height value 
        {
            if (cell.CellPosition.y < NeighbourHeights[i] && NeighbourHeights[i] != float.NaN)
            {
                higherDirection.Add(new Vector2(i, NeighbourHeights[i]));
            }
        }

        higherDirection.Sort((a, b) => a.y.CompareTo(b.y));             // Sort according to their height        HexDirection lowerDirection;        for (int i = 0; i < higherDirection.Count; i++)
        {
            lowerDirection = (HexDirection)(int)higherDirection[i].x;
            if (cell.GetNeighbor(lowerDirection) == null && cell.GetNeighbor(lowerDirection).type == HexType.Empty)
            {
                cell.Direction = lowerDirection;
                break;
            }
        }        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }    }
    



    /// <summary>    /// Will move in the same direction if it can. if not will popint left or right and then next loop move in that direction. if not will repeat until it can move.    /// </summary>    /// <param name="cell"></param>    /// <param name="num"></param>    public static void ChangeDirectionRandomly(HexCell cell, System.Random num)    {        if (cell.GetNeighbor(cell.Direction) != null && cell.GetNeighbor(cell.Direction).type != HexType.Empty)        {            int randomDirection = num.Next(2);            cell.Direction += randomDirection == 0 ? -1 : randomDirection;        }

        // Change direction if edge of map
        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }    }    /// <summary>    /// If there are empty cells surrounding, move randomly    /// </summary>    /// <param name="cell"></param> This is the cell which is being modified    /// <param name="typeFreq"></param> The types of the neighbours, either empty of water    /// <param name="NeighbourTypeDirections"></param> A list of different types with the direction of each of them    /// <param name="num"></param> A random number generator    public static void MoveRandomly(HexCell cell, int nEmptyNeighbours, HexDirection[,] NeighbourTypeDirections, System.Random num)    {        if (nEmptyNeighbours > 0)        {            int newIndex = num.Next(nEmptyNeighbours);                                      // Find a random neighbour, ie Select a number between 0 and the number of surrounding empty cells            cell.Direction = NeighbourTypeDirections[(int)HexType.Empty, newIndex];         // Find the direction of a random empty cell        }

        // Change direction if edge of map
        while (cell.GetNeighbor(cell.Direction) == null)        {            cell.Direction = (HexDirection)num.Next(0, 6);        }    }}