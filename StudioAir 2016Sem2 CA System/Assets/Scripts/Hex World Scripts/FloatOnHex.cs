﻿using UnityEngine;
using System.Collections;

public class FloatOnHex : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (HexCoordinates.iFp(transform.position) >= 0 && HexCoordinates.iFp(transform.position) < CAGridManager.TerrainHexGrid.Length)
        {
            Vector3 cellPos = CAGridManager.TerrainHexGrid[HexCoordinates.iFp(transform.position)].CellPosition;
            cellPos.x = transform.position.x;
            cellPos.z = transform.position.z;
            transform.position = cellPos;
        }

    }
}
