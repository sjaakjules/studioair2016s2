﻿using UnityEngine;
using System.Collections;

namespace CArules
{
    public class PartCRules : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }





        // ------------------------------------------------------------------------------------------------------------ //
        // ------------------------------------------------------------------------------------------------------------ //
        //                                       Don't Touch rules below                                                //
        // ------------------------------------------------------------------------------------------------------------ //
        // ------------------------------------------------------------------------------------------------------------ //




        public static void PlagueRules(HexCell cell, int[] typeFreq, HexDirection[,] NeighbourTypeDirections, System.Random num,int plantHealthPerTurn, int maxHealth)
        {

            cell.changeInHealth += plantHealthPerTurn;

            // If fully grown split into two if there is an empty cell as a neighbour
            if (cell.health >= maxHealth && typeFreq[(int)HexType.Empty] > 0)
            {
                // find the indicie of the new neighbour
                int newIndex = num.Next(typeFreq[(int)HexType.Empty]);
                // Make the empty cell a plant
                (cell.GetNeighbor(NeighbourTypeDirections[(int)HexType.Empty, newIndex])).changeType(HexType.Agent, maxHealth / 2);
                cell.changeInHealth += -cell.health / 2;
            }
        }

        public static void AgentRules(HexCell cell, int[] typeFreq, HexDirection[,] NeighbourTypeDirections, System.Random num, int bacteriaWalkingHealthPerTurn, int bacteriaEatingHealthPerTurn, int bacteriaPlantLifeDrain, int maxHealth)
        {

            // Change direction if edge of map
            while (cell.GetNeighbor(cell.Direction) == null)
            {
                cell.Direction = (HexDirection)num.Next(0, 6);
            }

            if (cell.GetNeighbor(cell.Direction).type != HexType.Agent && typeFreq[(int)HexType.Agent] > 0)
            {
                cell.Direction = NeighbourTypeDirections[(int)HexType.Agent, num.Next(typeFreq[(int)HexType.Agent])];
            }

            //  ////////////////////////////////////////////
            //  Do something in the direction it is facing
            switch (cell.GetNeighbor(cell.Direction).type)
            {
                case HexType.Empty:
                    // if there is an empty locations, move there
                    cell.changeInHealth += bacteriaWalkingHealthPerTurn;
                    cell.GetNeighbor(cell.Direction).ReplaceCell(cell);
                    break;
                case HexType.Agent:
                    // If there is a plant hex infront of it, eat it. If you have full health, split
                    cell.changeInHealth += bacteriaEatingHealthPerTurn;
                    cell.GetNeighbor(cell.Direction).changeInHealth += bacteriaPlantLifeDrain;
                    break;
                case HexType.plant:
                    // if it is a bacteria change direction
                    cell.changeInHealth += bacteriaWalkingHealthPerTurn;
                    do
                    {
                        cell.Direction = (HexDirection)num.Next(0, 6);
                    } while (cell.GetNeighbor(cell.Direction) == null);
                    break;
                default:
                    break;
            }

            // ////////////////////////////////////////////
            //       Split if it is fully grown
            if (cell.health >= maxHealth && typeFreq[(int)HexType.Empty] > 0)
            {
                // find the indicie of the new neighbour
                int newIndex = num.Next(typeFreq[(int)HexType.Empty]);
                // Make the empty cell a plant
                (cell.GetNeighbor(NeighbourTypeDirections[(int)HexType.Empty, newIndex])).changeType(HexType.plant, maxHealth / 2);
                cell.changeInHealth += -cell.health / 2;
            }
        }

        /// <summary>
        /// DON'T TOUCH
        /// This is julians testing script, copy this one and modify but don't edit!
        /// </summary>
        /// <param name="cell"></param> This is the cell which is being modified
        /// <param name="lowerFreq"></param> The number of cells lower than this one
        /// <param name="lowerNeighbour"></param> The array of hexDirections which are lower
        /// <param name="lowestNeighbour"></param> The neighbour which is lowest
        /// <param name="typeFreq"></param> The types of the neighbours, either empty of water
        /// <param name="lowestHeight"></param> The height of the lowest neighbour, Will be 9999f if there is nothing lower.
        /// <param name="NeighbourTypeDirections"></param> A list of different types with the direction of each of them
        /// <param name="num"></param> A random number generator
        public static void WaterRules(HexCell cell, int lowerFreq, HexDirection[] lowerNeighbour, HexDirection lowestNeighbour, int[] typeFreq, float lowestHeight, HexDirection[,] NeighbourTypeDirections, System.Random num)
        {
            if (lowestHeight != 9999f)   // Will be 9999 if this cell is the lowest
            {
                if (cell.GetNeighbor(lowestNeighbour).type == HexType.Water)
                {
                    cell.GetNeighbor(lowestNeighbour).changeInHealth += cell.health / 3;
                    cell.changeInHealth += -cell.health / 3;
                }
                else
                {
                    cell.GetNeighbor(lowestNeighbour).ReplaceCell(cell);
                }
            }
            else if (lowerFreq > 0)
            {
                if (cell.GetNeighbor(lowerNeighbour[num.Next(lowerFreq)]).type == HexType.Water)
                {
                    cell.GetNeighbor(lowestNeighbour).changeInHealth += cell.health / 3;
                    cell.changeInHealth += -cell.health / 3;
                }
                else
                {
                    cell.GetNeighbor(lowerNeighbour[num.Next(lowerFreq)]).ReplaceCell(cell);
                }
            }
            else if (typeFreq[(int)HexType.Empty] > 0)
            {
                // find the indicie of the new neighbour
                int newIndex = num.Next(typeFreq[(int)HexType.Empty]);
                // Make the empty cell a plant
                (cell.GetNeighbor(NeighbourTypeDirections[(int)HexType.Empty, newIndex])).ReplaceCell(cell);
            }
        }

    }
}

