﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class AlignNorthManager : MonoBehaviour
{

    // Orientation Variables
    public int orientationSamples = 30;
    public float simRotation;
    public bool simNorth;

    static float calibratedOrentation;
    float[] orientationCalibration;
    int calibrationCount = 0;
    public static bool orientationCalibrated = false;

    public static string errorText;

    //// External components
    //Text errorText;
    //GPSManager gps;
    //Transform rotateGameObject;

    //bool foundErrorText = false;
    //bool foundAlignNorth = false;
    //bool hasRotatedNorth = false;

    // Getter for a quaternion rotation which can be applied to gameObjects
    public static Quaternion alignNorthRotation { get { return Quaternion.Euler(0, calibratedOrentation, 0); } }
    public static Vector3 alignNorthEulerAngles {  get { return new Vector3(0, calibratedOrentation, 0); } }
    public static float alignNorthAngle { get { return calibratedOrentation; } }


    // Use this for initialization
    void Awake()
    {


        if (GPSManager.isSimulated)
        {
            simNorth = true;
        }

        orientationCalibration = new float[orientationSamples];

    }

    // Function to rotate gameobjects
    public static void RotateObjectNorth(Transform transform)
    {
        if (orientationCalibrated)
        {
            transform.Rotate(new Vector3(0, calibratedOrentation, 0));
        }
    }



    // Update is called once per frame
    void Update()
    {
        if (simNorth)
        {
            calibratedOrentation = simRotation;
            orientationCalibrated = true;
        }
        else if (!orientationCalibrated && calibrationCount < orientationSamples)
        {
            orientationCalibration[calibrationCount] = Input.compass.trueHeading;
            calibrationCount++;
        }
        else if (!orientationCalibrated)
        {
            orientationCalibrated = true;
            calibratedOrentation = orientationCalibration.Average();
        }


        errorText = string.Format("Calibrated Orientation: {0}\n" +
                                            "True Heading: {1}\n",
                                        calibratedOrentation, Input.compass.trueHeading);
        errorText = errorText + "\nHold Device Still, Calibrating...\n";
        
    }



}
