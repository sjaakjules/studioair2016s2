﻿//MAPNAV Navigation ToolKit v.1.3.4
//Attention: This script uses a custom editor inspector: MAPNAV/Editor/SetGeoInspector.cs

using UnityEngine;
using System.Collections;


public class SetGeo : MonoBehaviour
{
    public float lat;
    public float lon;

    public float orientation;
    public float scaleX;
    public float scaleY;
    public float scaleZ;

    private float initX;
    private float initZ;
    private bool gpsFix;
    private float fixLat;
    private float fixLon;

    void Awake()
    {
        //Reference to the MapNav.js script and gpsFix variable. gpsFix will be true when a valid location data has been set.
        gpsFix = GPSManager.isConnected;
    }

    IEnumerator Start()
    {
        //Wait until the gps sensor provides a valid location.
        while (!gpsFix)
        {
            gpsFix = GPSManager.isConnected;
            yield return null;
        }
        //Read initial position (used as a reference system)
        initX = GPSManager.iniRef.x;
        initZ = GPSManager.iniRef.z;
        //Set object geo-location
        GeoLocation();
    }
    

    void GeoLocation()
    {
        //Translate the geographical coordinate system used by gps mobile devices(WGS84), into Unity's Vector2 Cartesian coordinates(x,z) and set height(1:100 scale).
        transform.position = new Vector3((((lon * 20037508.34f) / 18000) - GPSManager.iniRef.x), transform.position.y, (((Mathf.Log(Mathf.Tan((90 + lat) * Mathf.PI / 360)) / (Mathf.PI / 180)) * 1113.19490777778f) - GPSManager.iniRef.z));

        //Set object orientation
        Vector3 tmp = transform.eulerAngles;
        tmp.y = orientation;
        transform.eulerAngles = tmp;

        //Set local object scale
        if (transform.localScale != Vector3.zero)
            transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
    }
    
}