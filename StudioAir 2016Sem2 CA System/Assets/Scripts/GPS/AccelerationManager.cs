﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class AccelerationManager : MonoBehaviour {

    // Acceleration Variables
    int accelerationSamples = 25;
    int accelerationCount = 0;
    float[] accelerationHistoryX, accelerationHistoryZ;//, velocityHistoryX, VelocityHistoryZ;
    float currentAccelerationX, currentAccelerationZ;//, currentVelocityX, currentVelocityZ;
    // Constant time Variables
    //float lastDeltaTime, deltaTime;
   float[] deltaTimeHisory;
    float velocityX, velocityZ;
    bool hasFilled = false;
    float[] windowedAccelX = new float[2];
    float[] WindowedAccelZ = new float[2];
  //  Vector3 velocity = Vector3.zero;


    GameObject phoneOrientation;
    //Text errorText;
    // Use this for initialization

    void Awake()
    {

       // errorText = GameObject.FindGameObjectWithTag("ErrorText").GetComponent<Text>();
        phoneOrientation = GameObject.FindGameObjectWithTag("MainCamera");

        accelerationHistoryX = new float[accelerationSamples];
        accelerationHistoryZ = new float[accelerationSamples];
       // velocityHistoryX = new float[accelerationSamples];
       // VelocityHistoryZ = new float[accelerationSamples];
        deltaTimeHisory = new float[accelerationSamples];


        /////////////////////////////////////////////////////////////////////////
        /////////////////////////Repeating methods///////////////////////////////
        InvokeRepeating("ConstantTime", 0.1f, 0.01f);
    }

    void Start () {
	
	}

    void ConstantTime()
    {
      //  deltaTime = Time.realtimeSinceStartup - lastDeltaTime;
        //lastDeltaTime = Time.realtimeSinceStartup;
        deltaTimeHisory[accelerationCount] = Time.realtimeSinceStartup;

        accelerationHistoryX[accelerationCount] = Input.acceleration.x;
        accelerationHistoryZ[accelerationCount] = Input.acceleration.z;

        float averageAccelX = 0;
        float averageAccelZ = 0;


        for (int i = 0; i < (hasFilled ? accelerationSamples : accelerationCount); i++)
        {
            averageAccelX += accelerationHistoryX[i];
            averageAccelZ += accelerationHistoryZ[i];
        }

        windowedAccelX[0] = windowedAccelX[1];
        windowedAccelX[1] = 1.0f * averageAccelX / (hasFilled ? accelerationSamples : accelerationCount + 1);

        WindowedAccelZ[0] = WindowedAccelZ[1];
        WindowedAccelZ[1] = 1.0f * averageAccelZ / (hasFilled ? accelerationSamples : accelerationCount + 1);

        /////////////////////////////////////////////////////////////////
        //////////////// Window average ////////////////////////////////

      //  currentVelocityX = currentVelocityX + 0.5f * (windowedAccelX[0] + windowedAccelX[1]) * deltaTime;
       // currentVelocityZ = currentVelocityZ + 0.5f * (WindowedAccelZ[0] + WindowedAccelZ[1]) * deltaTime;


        /*
         * 
         * /////////////////////////////////////////////////////////////
         * /////////////////// normal average not working //////////////
        // calculate the velocity using the average values over the average delta time
        // If it has filled the time differece will use the indicies of accelerationCount and the next one, thus if at end of array will be first (zeroth)
        // If it has not filled then the indicie for change in time will be current and the zeroth location
        if (hasFilled)
        {
            if (accelerationCount >= accelerationSamples - 1)
            {
                currentVelocityX = 0.5f * (averageAccelHistX[0] + averageAccelHistX[1]) * deltaTimeHisory[accelerationCount] - deltaTimeHisory[0];
            }
            else
            {
                currentVelocityX = 0.5f * (averageAccelHistX[0] + averageAccelHistX[1]) * deltaTimeHisory[accelerationCount] - deltaTimeHisory[0];
            }
        }
        else
        {

            currentVelocityX = 0.5f * (averageAccelHistX[0] + averageAccelHistX[1]) * deltaTimeHisory[accelerationCount] - deltaTimeHisory[0];
        }
         *
         *
         *
         */

        if (accelerationCount >= accelerationSamples)
        {
            accelerationCount = 0;
            hasFilled = true;
        }
    }
    // Update is called once per frame
    void Update () {

        Quaternion head = phoneOrientation.transform.rotation;

        Vector3 vx = head * Vector3.right;
        Vector3 vy = head * Vector3.up;
        Vector3 vz = head * Vector3.forward;

        Vector3 gravityVector = new Vector3(vx.y, vy.y, vz.y);
        gravityVector = gravityVector.normalized;
        /*
        AccelerationEvent[] accelerations = Input.accelerationEvents;
        Vector3 thisAccel;
       Vector3 lastAccel;

        for (int i = 1; i < accelerations.Length; i++)
        {
            lastAccel = new Vector3(accelerations[i - 1].acceleration.x + (gravityVector.x * 0.98f),
                                            accelerations[i - 1].acceleration.y + (gravityVector.y * 0.98f),
                                            accelerations[i - 1].acceleration.z - (gravityVector.z * 0.98f));
            thisAccel = new Vector3(accelerations[i].acceleration.x + (gravityVector.x * 0.98f),
                                            accelerations[i].acceleration.y + (gravityVector.y * 0.98f),
                                            accelerations[i].acceleration.z - (gravityVector.z * 0.98f));
            Vector3 newVel = ((accelerations[i - 1].acceleration + gravityVector * 0.98f) + (accelerations[i].acceleration + gravityVector * 0.98f)) * (0.5f * accelerations[i].deltaTime);

            if (newVel.magnitude > 0.005)
            {
                velocity = velocity + newVel;
            }
        }


    */

        //velocityX = accelerations.Average(accel => accel.acceleration.divide(deltaTime).x);
        //velocityZ = accelerations.Average(accel => accel.acceleration.divide(deltaTime).z);
    }
}
