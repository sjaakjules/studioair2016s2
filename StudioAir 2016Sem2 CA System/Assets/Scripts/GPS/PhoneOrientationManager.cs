﻿using UnityEngine;
using System.Collections;

public class PhoneOrientationManager : MonoBehaviour {

    bool foundPhoneOrientation = false;
    public Transform phoneOrientation;

    public Vector3 Up
    {
        get
        {
            if (foundPhoneOrientation)
            {
                return phoneOrientation.up;
            }
            return Vector3.zero;
        }
    }
    public Vector3 Right
    {
        get
        {
            if (foundPhoneOrientation)
            {
                return phoneOrientation.right;
            }
            return Vector3.zero;
        }
    }
    public Vector3 Forward
    {
        get
        {
            if (foundPhoneOrientation)
            {
                return phoneOrientation.forward;
            }
            return Vector3.zero;
        }
    }


    void Awake()
    {
        try
        {
            phoneOrientation = GameObject.FindGameObjectWithTag("PhoneTransform").transform;
        }
        catch (System.Exception)
        {
        }

        if (phoneOrientation != null)
        {
            foundPhoneOrientation = true;
        }

    }
}
