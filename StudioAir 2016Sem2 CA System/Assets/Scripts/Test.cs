﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

	public Vector3 dir;
	public Vector3 scale;
	public float speed;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//transform.position = dir;
		//transform.position = transform.position + dir;	

		transform.localScale = scale;
		//transform.localScale = transform.localScale + scale;

		transform.Translate (dir * speed * Time.deltaTime);

		if (transform.position.y > 10)
			{
				dir = -dir;
			}
	}
}
